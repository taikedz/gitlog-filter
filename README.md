# Git-Log Filter

Advanced git log filtering.

A command line for searching and comparing at a basic level is available, see Historic Modes.

Define filter chains in a YAML file, and the name of a filter chain; run in a Git current working directory, and see the filter in action!

```sh
# A small example

python3 glf/glf.py filter -F filter_example.yaml printtest
```

Requires PyYAML.

## Filter Chains

Describe a chain of filters to apply to a git log stream, and print the result. A simple example:

```yaml
load:
  - action: load
    into: current_progress
    earlier: master
    later: dev

fixes:
  - action: search
    from: current_progress
    into: fixes_and_features
    terms:
    - bugfix/
    - feature/
    ignore_case: true

  - action: message
    message: "----- Fixes + features commits"

  - action: print_all
    from: fixes_and_features

cves:
  - action: message
    message: "---------- CVEs --"

  - action: extract
    from: current_progress
    into: CVE_reports
    pattern: ".*?(CVE-[0-9-]+\\s+.+)"
    group: 1

  - action: message
    message: "----- CVE-related commits"

  - action: print_all
    from: CVE_reports
```

This can then be called with the appropriate filter chains:

```sh
# Just print fixes
python3 glf/glf.py -F filter_def.yaml load fixes

# Just print CVE reports
python3 glf/glf.py -F filter_def.yaml load cves

# Print both
python3 glf/glf.py -F filter_def.yaml load cves fixes
```

Custom chain filters are easy to define, using a single function as entry point, and a utility to access to the runtime commits store.

See [the filter spec](Filter_spec.md) for information on the current modules.

See the [example file](filter_example.yaml) for a larger example.

## Historic Modes

Given a git log stream, search for commits, or filter out commits, based on search terms.

* Use the `search` mode to find specific commits, optionally printing hashes to a file using `>`/`>>` redirection
* Use the `-x` exclusion mode to print only commits that do not match any of the search terms
* Use the compare mode to see the differences between a new branch and an old branch, and exclude any duplicates that came in from merging the same external branch

Try `glf.py search -h` and `glf.py compare -h` for specific help.

## Tips

If you want to print just a specific commit entry by a tag name, use

```
glf.py search -F annotations $TAG_NAME
```

You can then restrict your search over a subset of recent commits:

```
glf.py search -S $LIMIT_HASH -i $SEARCH_TERMS
```

## Removing double-merged commits from the listing

If you are comparing two branches which you know have received the same merge in separate operations from the same source, use the `compare` mode.

Run `glf.py compare --ancestor $C_ANCESTOR --old-branch $C_OLD --new-branch $C_NEW TERMS ...`

where the `$C_*` are branch names, tags, or hashes.
