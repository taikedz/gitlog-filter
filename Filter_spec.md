# Filter Spec Format Definition

Filter chains are specified as entries in a YAML file.

At the top level, individual filter chain names are specified, under which an array of key-value sets represent each filter

```yaml
filter_chain_name:
  - action: action_name
    param: value
    param2: value

  - action: next_action_name
    param: more
    param2: params
```

Some parameters are defined with a common meaning:

* `into` : named store into save the loaded sequence of commits - not used by `print_all` action
* `from` : named store from which to read a sequence of commits - not used by `load` action

Each action represents a filter. The following actions are currently impemented:

## `load`

Runs `git log` in the current working directory, and parses it.

Parameters:

* `start` - the earliest commit to load.
* `finish` (default `HEAD`) - the most recent commit, or a commit to get a log difference from relative to `finish`
* `file` - (optional) path to a file to load git log data from, instead of target repository. If specified, `start` and `earlier` are ignored.

## `search`

Checks whether specific terms are found in the git log.

Results are stored in the name specified in `into`

Parameters:

* `terms` - an array of terms to find
* `match_all` (default `false`) - whether to require all terms to be present, or if any one term will be enough.
* `ignore_case` (default `false`) - whether to ignore case differences when matching
* `exclude_mode` (default `false`) - whether to retain only items not matched by the search (`True`)
* `field` (default `message`) - search on the commit message field. Other options are `title`, `hash`, `annotations` (tags, branch names, etc), `date` and `author`

## `extract`

Search using a regular expression.

Results are stored in the name specified in `into`

* `pattern` - a single pattern to match against message lines of a commit
* `group` (optional) - if specified as an `int`, prints that capturing group during filtering

## `extract_references`

Search using a regular expression - the regex must capture a commit hash. The corresponding entry will be loaded from the `from` list.

Results are stored in the name specified in `into`

Parameters:

* `pattern` - a single pattern to match against message lines of a commit
* `group` - the `int` representing the capturing group containing exactly one commit hash/short hash

## `deduplicate`

Given a main set of commits, and a secondary set, remove from main any commits that match those in secondary.

Results are stored in the name specified in `into`

Parameters:

* `duplicates` - the name of a stored set of commits
* `ignore_hash` (default `false`) match only on metadata content (diff content is never cmopared)

## `combine`

Combine two lists into a target list.

Results are stored in the name specified in `into`

Parameters:

* `with` - the name of a list to combine with the `from` list

## `print_all`

Prints all the commits in a given stored commit set.

Parameters:

* `mode` (default `full`) - whether to print a `full` commit entry, a `oneline` version with a short hash and commit title, or just the `hashes`

## `write`

Writes a commit sequence to a file.

Parameters:

* `file` - the path of the file to write to 

## `message`

Simply prints a message, for separating filter outputs. Does not use `from` or `into`

# Writing more filters

Filters are stored in `glf/filters`. Their file name, minus the extension, is their name in action fields, and must only contain letters, numbers and underscores.

A filter needs only to provide a `execute(step_data)` function, where `step_data` is the configuration as received from the YAML configuration. A `filters/fitler_utils.py` utility can be used for accessing the commit list store for retrieving and writing commit lists. The store must only receive commit lists.
