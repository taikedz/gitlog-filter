import os
import sys
import logging

import gitlog
import argumenthandler
import commits
import compare
import errors as GE
import filtering
import search



def main():
    try:
        setup_logging()

        action = sys.argv[1]
        arguments_raw = sys.argv[2:]
        args = argumenthandler.parse_action(action, arguments_raw)

        if action == "filter":
            filtering.process(args)
        else:
            all_terms = collect_terms(args)
            commit_list = build_commit_list(action, args)
            filtered_list = search.filter_terms(commit_list, all_terms, args.search_field, args.case_insensitive)
            print_results(filtered_list, args.print_mode)

    except GE.GLFError as e:
        logging.error(e)
        return e.code

    return 0


def setup_logging():
    setup = {
            "level" : getattr(logging, os.getenv("PYTHON_LOGGING", "INFO")),
            "stream" : sys.stderr
            }
    logging.basicConfig(**setup)



def print_results(commit_list, print_mode):
    for entry in commit_list:
        entry.print(print_mode)


def build_commit_list(action, args):
    if action == "search":
        return commits.read_commits(gitlog.get_stream(), args.stop_commit )

    elif action == "compare":
        return compare.get_git_log_without_ancestor(args.ancestor_branch, args.old_branch, args.new_branch)

    else:
        raise GE.InvalidAction(f"Invalid action '{action}'")


def collect_terms(args):
    all_terms = args.terms

    if args.search_terms_file:
        with open(args.search_terms_file, 'r') as fh:
            all_lines = fh.readlines()
            all_lines = [line.strip() for line in all_lines]
            all_terms.extend(all_lines)

    elif len(all_terms) < 1:
        all_terms = None
        #raise GE.SearchTermsError("No search terms provided")

    return all_terms

if __name__ == "__main__":
    code = main()
    if code:
        exit(code)
