import os

# Generic Error

ERROR_GENERIC = 127

class GLFError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)
        self.code = ERROR_GENERIC
        self.parents = []

    def add_parent(self, e):
        self.parents.append(e)

    def __str__(self):
        messages = []
        if self.parents:
            for e in self.parents:
                messages.append(Exception.__str__(e))
        messages.append(Exception.__str__(self))

        return os.linesep.join(messages)

# Implementation errors


class ImplementationError(GLFError):
    def __init__(self, message):
        ImplementationError.__init__(self, message)
        self.code = 126


class CommitConcluded(ImplementationError):
    pass

class CommitListDuplicationError(ImplementationError):
    pass


# User errors

class SearchTermsError(GLFError):
    def __init__(self, message):
        GLFError.__init__(self, message)
        self.code = 10


class InvalidAction(GLFError):
    def __init__(self, message):
        GLFError.__init__(self, message)
        self.code = 11


class EnvError(GLFError):
    def __init__(self, message):
        GLFError.__init__(self, message)
        self.code = 12


class PrintmodeError(GLFError):
    def __init__(self, message):
        GLFError.__init__(self, message)
        self.code = 13


class YamlConfigError(GLFError):
    def __init__(self, message):
        GLFError.__init__(self, message)
        self.code = 14

class StoreError(GLFError):
    def __init__(self, message):
        GLFError.__init__(self, message)
        self.code = 15
