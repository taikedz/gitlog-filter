def filter_terms(commit_list, all_terms, field="message", ignore_case=False, exclude_mode=False, match_all=False):
    filtered_list = []

    for entry in commit_list:
        if all_terms is None:
            filtered_list.append(entry)

        elif entry.has_terms(all_terms, field=field, ignore_case=ignore_case, match_all=match_all):
            if not exclude_mode:
                filtered_list.append(entry)

        else:
            if exclude_mode:
                filtered_list.append(entry)

    return filtered_list
