import os
import re
import sys
import logging

import console
import errors as GE


# Similarity check between two commits
# CHECK_HASH:
# - True:  commits should have same hash and thus be identical
# - False: commits should have the same metadata, but the hash can be different
#          indicating a cherry-pick copy
#
# VERY DIRTY -- file-global variable !!
# But needed for each CommitEntry instance to refer to
#   and can change over running time of script
CHECK_HASH = True

PRINT_ANNOTATIONS = True


def linestrip(line):
    return re.sub("(\\r\\n|\\r|\\n)$", '', line)


def read_commits(stream, until=None):
    commit_chain = []
    current_commit = CommitEntry()
    current_line = stream.readline()

    while current_line != '':
        current_commit.add_line(linestrip(current_line) )
        current_line = stream.readline()

        if re.match("^commit ", current_line) or current_line == '':
            current_commit.conclude()
            commit_chain.append(current_commit)
            current_commit = CommitEntry()

            if until and current_line.startswith("commit "+until):
                break

    return commit_chain


class CommitMap(dict):
    """ dict representation of a set of commits using
    hashes as keys.

    Allows looking up by any short-hash (minimum 7 in length)
    """

    def __init__(self, commit_list):
        for entry in commit_list:
            self.__setitem__(entry.hash, entry)

    def __getitem__(self, subhash):
        if len(subhash) == 40: # Exact length of a commit hash
            return super().__getitem__(subhash)

        if len(subhash) < 7: # Shortest subhash that 'git log --oneline' will output
            raise KeyError(f"Short hashes cannot be fewer than 8 characters long '{subhash}' length is {len(subhash)}")

        for entry in self.values():
            if entry.hash.startswith(subhash):
                return entry

        raise KeyError(f"Could not find commit {subhash}")


class CommitEntry:
    def __init__(self):
        self.hash = None
        self.author = None
        self.date = None
        self.merge = None
        self.message = []
        self.title = None
        self.annotations = None
        self.concluded = False


    def __str__(self):
        commit_line = [self.hash]
        if PRINT_ANNOTATIONS:
            commit_line.append(self.annotations)
        commit_line = " ".join(commit_line)

        components = [
            "commit %s"%(commit_line),
            "Author: "+self.author,
            "Date: "+self.date,
        ]

        if self.merge:
            components.append(self.merge)

        components.append(self.message)

        return os.linesep.join(components)


    def __eq__(self, other):
        """ The same commit, where the contents and the hash are identical
        
        Does not check the associated diffs.
        """
        res = True
        if CHECK_HASH:
            res = (res and self.hash == other.hash)
        res = (res and self.sameAs(other))
        return res


    def sameAs(self, other):
        """ The commits are the same, notwithstanding the hash.

        This does not compare the diffs, just the header contents.
        """
        for attr in ['concluded', 'author', 'date', 'message', 'merge']:
            if getattr(self, attr) != getattr(other, attr):
                return False
        return True


    def add_line(self, line):
        if self.concluded:
            raise GE.CommitConcluded("Cannot add data - commit entry is concluded.")

        m = re.match("^(commit|Author:|Date:|Merge:)\s*(.+)", line)

        if not m:
            self.add_message(line)

        elif m.group(1) == "commit":
            commit_parts = re.match("^([a-f0-9]+)\s*(.*)$", m.group(2))
            self.hash = commit_parts.group(1)
            self.annotations = commit_parts.group(2)

        elif m.group(1) == "Author:":
            self.author = m.group(2)

        elif m.group(1) == "Date:":
            self.date = m.group(2)

        elif m.group(1) == "Merge:":
            self.merge = "Merge: {}".format( m.group(2) )


    def add_message(self, line):
        if( re.match('^\s*$', line) and
                self.title == None and
                len(self.message) > 0): # there is a blank line before the message body
            self.set_title(" ".join(self.message))

        self.message.append(line)


    def set_title(self, title):
        self.title = re.sub('^\s+', '', title)
        logging.debug("TITLE SET >> %s" % self.title)


    def conclude(self):
        self.concluded = True

        if self.message[-1] != '':
            # Final commit must have same empty line as other commits get
            self.message.append('')
        self.message = os.linesep.join(self.message)

        if not self.title:
            self.set_title(self.message)


    def print(self, print_mode):
        if print_mode == "full":
            output = str(self)

        elif print_mode == "oneline":
            output = "{} {}".format(self.hash[:8], self.title )

        elif print_mode == "hashes":
            output = self.hash[:10]

        else:
            raise GE.PrintmodeError(f"Invalid print mode '{print_mode}'")

        try:
            console.print(output)

        except UnicodeEncodeError as e:
            # Printing Unicode characters in Windows is broken
            # Make error visible in actual output (else seems silent)
            console.print(">> UNICODE ERROR printing {}".format(self.hash))
            raise GE.EnvError("You need to run \n\n\tset PYTHONIOENCODING='utf-8'\n\n in your environment before running this script.")

        except OSError as e:
            logging.error(e)
            logging.error("Occurred on {}".format(self.hash))


    def has_terms(self, terms, field, ignore_case=False, match_all=False):
        target = getattr(self, field)
        found = False

        if target is None:
            return False

        for term in terms:
            if ignore_case:
                term = term.lower()
                target = target.lower()

            if term in target:
                found = True
            else:
                if match_all:
                    return False

            if found:
                if not match_all: # Ergo, match any
                    return True

        # Match-all and found true, return variable status anyway
        return found
