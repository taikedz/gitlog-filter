import logging

import console
import filters.filter_utils as utils

def execute(step_data):
    utils.require_parameters(["message"], step_data)

    message = step_data["message"]
    log_level = utils.read_conf(step_data, "log", None)

    if log_level is None:
        comsole.print(message)

    else:
        log_level = getattr(logging, log_level.upper())
        logging.log(log_level, message)
