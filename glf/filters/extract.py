import re
import os

import filters.filter_utils as utils
import filters.filter_errors as FE

def execute(step_data):
    utils.require_parameters(["from", "pattern"], step_data)

    pattern = re.compile(step_data["pattern"])

    group = int(utils.read_conf(step_data, "group", -1))
    into = utils.read_conf(step_data, "into", None)

    commit_list = utils.get_store(step_data["from"])
    result_list = []

    for entry in commit_list:
        matched_entry = search_pattern(pattern, entry, group)

        if matched_entry:
            result_list.append(matched_entry)

    if into:
        utils.set_store(into, result_list)


def search_pattern(pattern, entry, group, handle=print):
    """ Search for pattern on each line individually,
    since re.MULTILINE and re.DOTALL do not allow for
    the search we need.
    """
    lines = entry.message.split(os.linesep)
    found = False

    for line in lines:
        matched = re.match(pattern, line)

        while matched:
            found = True
            if group > -1:
                handle(matched.group(group) )

            line = line[len(matched.group(0)):]
            matched = re.match(pattern, line)

    if found:
        return entry
