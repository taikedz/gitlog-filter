import re

import commits
import filters.extract
import filters.filter_utils as utils

def execute(step_data):
    utils.require_parameters(["from", "into", "lookup_in", "pattern", "group"], step_data)

    pattern = re.compile(step_data["pattern"])
    group = int(utils.read_conf(step_data, "group", -1))

    commit_list = utils.get_store(step_data["from"])
    lookup_in = utils.get_store(step_data["lookup_in"])
    result_list = []

    commit_map = commits.CommitMap(commit_list)

    def handler(found_hash):
        try:
            found_entry = commit_map[found_hash]

            if not found_entry in result_list:
                result_list.append(found_entry)

        except KeyError:
            # Found a hash referencing a commit that
            #  is not in our 'from' list; this is
            #  acceptable when processing sub-lists
            pass

    for entry in lookup_in:
        matched_entry = filters.extract.search_pattern(pattern, entry, group, handler)

    utils.set_store(step_data["into"], result_list)
