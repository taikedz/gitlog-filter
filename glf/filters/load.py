import gitlog
import commits as gitcommits
import filters.filter_utils as utils

def execute(step_data):
    commits = None
    filename = utils.read_conf(step_data, "file", None)
    utils.require_parameters(["into"], step_data)

    if filename:
        with open(filename, 'r') as commit_stream:
            commits = gitcommits.read_commits(commit_stream)

    else:
        utils.require_parameters(["start"], step_data)
        start = step_data["start"]
        finish = utils.read_conf(step_data, "finish", "HEAD")

        commit_stream = gitlog.get_difference(start, finish)
        commits = gitcommits.read_commits(commit_stream)

    utils.set_store(step_data["into"], commits)
