""" Idempotent filter that reads a commit list, and writes it to a file
"""

import os

import commits as gitcommits
import filters.filter_utils as utils

def execute(step_data):
    utils.require_parameters(["from", "file"], step_data)

    gitcommits.PRINT_ANNOTATIONS = utils.read_conf(step_data, "annotations", True)

    commits = utils.get_store(step_data["from"])
    filename = step_data["file"]

    with open(filename, 'wb') as fh:
        for item in commits:
            data = f"{item}{os.linesep}"
            fh.write(bytes(data, 'utf-8'))

