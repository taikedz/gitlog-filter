""" Idempotent filter that reads a commit list, and prints it out.
"""

import commits as gitcommits
import filters.filter_utils as utils

def execute(step_data):
    utils.require_parameters(["from"], step_data)

    commits = utils.get_store(step_data["from"])
    mode = utils.read_conf(step_data, "mode", "full")

    gitcommits.PRINT_ANNOTATIONS = utils.read_conf(step_data, "annotations", True)

    for item in commits:
        item.print(mode)

