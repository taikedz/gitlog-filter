import logging

import search
import filters.filter_utils as utils
import filters.filter_errors as FE

def execute(step_data):
    utils.require_parameters(["from", "into", "terms"], step_data)

    all_terms = term_extraction(step_data["terms"])

    search_parameters = {
        "field" : utils.read_conf(step_data, "field", "message"),
        "exclude_mode" : utils.read_conf(step_data, "exclude_mode", False),
        "ignore_case" : utils.read_conf(step_data, "ignore_case", False),
        "match_all" : utils.read_conf(step_data, "match_all", False),
    }

    commit_list = search.filter_terms(utils.get_store(step_data["from"]), all_terms, **search_parameters)
    utils.set_store(step_data["into"], commit_list)

def term_extraction(raw_terms):
    if type(raw_terms) is list:
        return raw_terms

    elif type(raw_terms) is str:
        if raw_terms.startswith("@"):
            return read_terms_file(raw_terms[1:])
        else:
            return get_field_terms(raw_terms)

    else:
        raise FE.FilterChainError("Bad type for terms - expected list of terms, or field lookup on stored entries  e.g. 'mystore/hash'")

def read_terms_file(file_name):
    with open(file_name, 'r') as fh:
        all_lines = fh.readlines()

    all_lines = list([line.strip() for line in all_lines])

def get_field_terms(raw_terms):
    store_name, field_name = raw_terms.split("/", 1)
    extracted_terms = []

    commit_list = utils.get_store(store_name)
    for entry in commit_list:
        extracted_terms.append(getattr(entry, field_name))

    logging.debug(extracted_terms)

    return extracted_terms
