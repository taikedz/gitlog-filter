import filters.filter_errors as FE
import errors as GE

COMMIT_STORE = {}

def get_store(name):
    global COMMIT_STORE

    try:
        return COMMIT_STORE[name].copy()

    except KeyError as e:
        raise GE.StoreError(f"No such stored item '{name}'")


def set_store(name, value):
    global COMMIT_STORE

    if not value:
        value = []

    COMMIT_STORE[name] = value


def read_conf(conf, name, default=None):
    if name in conf.keys():
        return conf[name]

    return default


def require_parameters(parameters, conf):
    conf_keys = conf.keys()
    for param in parameters:
        if param not in conf_keys:
            raise FE.FilterChainError(f"No parameter '{param}' in config {str(conf)}")
