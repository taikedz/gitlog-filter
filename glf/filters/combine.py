import filters.filter_utils as utils

def execute(step_data):
    utils.require_parameters(["from", "into", "with"], step_data)

    from_list = utils.get_store(step_data["from"])
    with_list = utils.get_store(step_data["with"])

    for entry in with_list:
        if entry not in from_list:
            from_list.append(entry)

    utils.set_store(step_data["into"], from_list)
