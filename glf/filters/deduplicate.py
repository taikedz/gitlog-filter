import compare
import commits
import filters.filter_utils as utils

def execute(step_data):
    utils.require_parameters(["from", "into", "duplicates"], step_data)

    main_line = utils.get_store(step_data["from"])
    dupe_line = utils.get_store(step_data["duplicates"])

    retain_duplicates = utils.read_conf(step_data, "retain_duplicates", False)
    ignore_hash = utils.read_conf(step_data, "ignore_hash", False)
    commits.CHECK_HASH = not ignore_hash

    fixed = compare.excise_duplicates(main_line, dupe_line, retain_duplicates)

    utils.set_store(step_data["into"], fixed)
