import subprocess
import sys
import os
import io


REPO_LOCATION = None


def get_difference(from_commit, to_commit):
    return get_stream("{}...{}".format(from_commit, to_commit))


def get_stream(target=None):
    command = ["git", "log", "--decorate"]

    if target:
        command.append(target)

    p = read_git_log(command)

    return io.StringIO(p.stdout.decode(sys.getdefaultencoding()) )


def read_git_log(command):
    here = None
    if REPO_LOCATION:
        here = os.path.abspath(os.curdir)
        os.chdir(REPO_LOCATION)

    p = subprocess.run(command, capture_output=True)

    if here:
        os.chdir(here)

    return p


def repo_location(path=None):
    global REPO_LOCATION

    if path:
        REPO_LOCATION = os.path.abspath(path)
    else:
        return REPO_LOCATION
