import importlib
import yaml

import gitlog
import commits
import errors as GE
import filters.filter_errors as FE


LOADED_FILTERS = {}

# Entrypoint to the filtering mode from standalone
def process(args):
    with open(args.filter_conf, 'r') as fh:
        y_conf = yaml.full_load(fh)

    if args.target_repo:
        gitlog.repo_location(args.target_repo)

    for chain_name in args.filter_chain:
        if chain_name not in y_conf.keys():
            raise GE.YamlConfigError(f"Filter chain '{chain_name}' was not found in <{args.filter_conf}>")

        filter_steps = y_conf[chain_name]

        for step in filter_steps:
            run_filter(step)


def run_filter(step_data):
    global LOADED_FILTERS

    filter_name = step_data["action"]

    if filter_name not in LOADED_FILTERS.keys():
        LOADED_FILTERS[filter_name] = load_filter(filter_name)

    filter_m = LOADED_FILTERS[filter_name]
    filter_m.execute(step_data)


def load_filter(filter_name):
    try:
        return importlib.import_module("filters."+filter_name)

    except ModuleNotFoundError as e:
        err = FE.FilterChainError(f"Error importing module filters.{filter_name}")
        err.add_parent(e)
        raise err
