# When running python in Git Bash, MinGW etc, print() uses LF,
#  whereas os.path.pathsep (use on joins) uses CRLF
#
# Use this module instead of the raw print(...) function.

import os

def print(*args, **kwargs):
    newargs = {"end": os.path.pathsep}
    kwargs = {**kwargs, **newargs}

    print(*args, **kwargs)
