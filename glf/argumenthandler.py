import argparse
import errors as GE
import commits

def parse_action(action, arglist):
    parsed_args = None

    if action == "search":
        parsed_args = searchmode_parse(arglist)

    elif action == "compare":
        parsed_args = comparator_parse(arglist)

    elif action == "filter":
        parsed_args = chain_filter_parse(arglist)

    else:
        raise GE.InvalidAction(f"'{action}' is not a recognised action.")

    return parsed_args


def searchmode_parse(arglist):
    parser = argparse.ArgumentParser()

    _add_searchmode_parameters(parser)

    parsed_args = parser.parse_args(arglist)

    return parsed_args


def comparator_parse(arglist):
    parser = argparse.ArgumentParser()

    _add_searchmode_parameters(parser)
    _add_comparator_parameters(parser)

    parsed_args = parser.parse_args(arglist)
    commits.CHECK_HASH = not parsed_args.imperfect_comparison

    return parsed_args


def chain_filter_parse(arglist):
    parser = argparse.ArgumentParser()

    parser.add_argument("-F", dest="filter_conf",
            help="YAML filter file")

    parser.add_argument("-T", dest="target_repo", default=None,
            help="Path to target repository to load logs from")

    parser.add_argument(dest="filter_chain", nargs="+",
            help="Filter chain(s) to run")

    parsed_args = parser.parse_args(arglist)
    return parsed_args


def _add_searchmode_parameters(parser):
    parser.add_argument("-x", dest="exclude_mode", action="store_true", default=False,
            help="Print only commits that did not match the search terms")

    parser.add_argument("-m", dest="print_mode", default="full",
            help="Print mode, can be 'full', 'oneline' or 'hashes'; 'full' by default.")

    parser.add_argument("-F", dest="search_field", default="message",
            help="Search specific field - can be any of 'hash', 'annotations', 'author', 'date', 'title', 'message' (default is 'message')")

    parser.add_argument("-l", dest="search_terms_file", default=None, type=str,
            help="Load additional search patterns from file, one per line")

    parser.add_argument("-i", dest="case_insensitive", action="store_true", default=False,
            help="Case-insensitive search")

    parser.add_argument("-S", dest="stop_commit", default=None, type=str,
            help="Stop at and exclude specified commit hash")

    parser.add_argument(dest="terms", nargs='*',
            help="Search terms")


def _add_comparator_parameters(parser):
    parser.add_argument("--new-branch", dest="new_branch",
            help="The newer branch of comparison")

    parser.add_argument("--old-branch", dest="old_branch",
            help="The older branch of comparison")

    parser.add_argument("--ancestor", dest="ancestor_branch",
            help="The common ancestor commit")

    parser.add_argument("-I", dest="imperfect_comparison", action="store_true", default=False,
            help="When comparing commit metadata, do not compare hashes (cherrypicked commits are considered duplicates)")
