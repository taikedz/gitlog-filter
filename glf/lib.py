import glf.filtering
import glt.filters.filter_utils


# === Use as a Library ---------------
# Convenience methods

def run_filter_file(args_dict):
    """ To use the filtering utility as a library, pass the following data in a dictionary to this function

    {
      "filter_conf": <str: file path> -- the filter configuration file
      "filter_chain": <list:str: chain names> -- the names of the filter chains from the config to execute
      "target_repo": <str: repo path> -- the path to the repo to load from (default is current working dir)
    }
    """
    class AttributableDict(dict):
        """ Use a dictionary to mock the parsed arguments object
        """
        pass

    m = AttributableDict(args_dict)

    for k,v in args_dict.items():
        setattr(m, k, v)

    glf.filtering.process(m)



def run_filter(step_data):
    """ Run a filter using a dictionary representing step data

    step_data = {
        "action": "load",
        "into": "my_store",
        "file": "my_file.yaml",
    }

    run_filter(step_data)
    """
    glf.filtering(step_data)


def get_commit_store(store_name):
    """ Return a list of CommitEntry objects as loaded by filters.
    """
    return glf.filters.filter_utils.get_store(store_name)
