import logging

import commits
import gitlog

def excise_duplicates(main_list, historic_list, retain_duplicates=False):
    """ Given a main list of commits, check a historic list for any duplicate commits.

    Return a new list of commits with duplicates found from historic list removed.
    """
    new_list = []
    for commit in main_list:
        logging.debug("Checking {}".format(commit.hash[:8]))

        if commit in historic_list:
            if retain_duplicates:
                new_list.append(commit)
            else:
                logging.debug("Excluded duplicate {}".format(commit.hash[:8]))

        else:
            if not retain_duplicates:
                new_list.append(commit)

    return new_list

def get_git_log_without_ancestor(ancestor_commit, old_commit, new_commit):
    ancestor_stream = gitlog.get_difference(ancestor_commit, old_commit)
    main_stream = gitlog.get_difference(old_commit, new_commit)

    ancestor_list = commits.read_commits(ancestor_stream)
    main_list = commits.read_commits(main_stream)

    for commit in ancestor_list:        logging.debug("ACNESTOR {}".format(commit.hash[:8]) )
    for commit in main_list:            logging.debug("MAIN     {}".format(commit.hash[:8]) )

    excised_list = excise_duplicates(main_list, ancestor_list)

    return excised_list
