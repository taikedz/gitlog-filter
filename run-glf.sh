#!/usr/bin/env bash

# Retain the current location of the script, so
#  as not to change working directory to run it
HERE="$(dirname "$0")"

# Mandatory environment variable for
#  UTF-8 string conversions to work
export PYTHONIOENCODING="utf-8"

# Runs the script at the right location
#  whilst retainig the user's current working dir
python3 "$HERE/glf/glf.py" "$@"
