# Use as a library

Arguably the most useful feature as a library would be the chain filters. See [the filter specification](Filter_spec.md) for expected parameters for each filter.

Use `import glf.lib` to access the base API

Using a definition from the filter specification, use `run_filter(step_data)` to run a filter. `step_data` itself is a 1-to-1 match against the filter definition, so for example performing a search:

```python
import glf.lib

my_step = {
    "action": "search",
    "from": "current_progress",
    "into": "reversions",
    "terms": ["revert"], # Terms is a list
    "ignore_case": True,
}

glf.lib.run_filter(my_step)
```

Alternatively, if you have your filters defined in a YAML file, you can perform the following

```python
import glf.lib

runtime_def = {
  "filter_conf": "my_filters.yaml"    # the filter configuration file
  "filter_chain": ["load", "cleanse"] # the names of the filter chains from the config to execute
  "target_repo": "/path/to/my/repo"   # the path to the repo to load commits from (default is current working dir)
}

glf.lib.run_filter_file(runtime_def)

```

## CommitEntry objects

To obtain a list of commits that has been stored by filters, retrieve it with `glf.lib.get_commit_store(store_name)`. This returns a Python `list` or `CommitEntry` items, in git-log order (typcially, first entry would be the tip of a branch).

* Each `CommitEntry` has the following properties: hash, author, date, merge, message, title, annotations
* The `title` is the first lines of the message until the first blank line
* The `annotations` are branch names, tag names, etc, as text
* The `message` and `title` are joined around the system newline `os.path.linesep`
